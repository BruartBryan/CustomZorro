import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'cz-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Input() backgroundColor?: string;
  @Input() color: string = '#fff';
  @Input() borderRadius?: number;
  @Input() loading?: boolean;
  @Input() outLined?: boolean;
  @Input() size: 'small' | 'medium' | 'large' = 'medium';
  @Input() linearGradient?: string;
  @Output() clickButton = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  onClick(event: any) {
    this.clickButton.emit(event);
  }
}
