import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cz-subtitle',
  templateUrl: './subtitle.component.html',
  styleUrls: ['./subtitle.component.scss'],
})
export class SubtitleComponent implements OnInit {
  @Input() color = '#000';
  @Input() underlined = false;
  @Input() fontWeightValue = 400;
  style = {};

  constructor() {}

  ngOnInit(): void {
    this.style = {
      textDecoration: this.underlined ? 'underline' : 'none',
      color: this.color,
      fontWeight: this.fontWeightValue,
    };
  }
}
