import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cz-paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.scss'],
})
export class ParagraphComponent implements OnInit {
  @Input() bold: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
