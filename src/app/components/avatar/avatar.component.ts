import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cz-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit {
  @Input() displayName: string = '';
  @Input() nzIcon: string = '';
  @Input() imgSrc: string = '';
  @Input() shape: 'circle' | 'square' = 'circle';
  @Input() customSize: 'small' | 'large' | number = 'small';
  @Input() customStyle: any;

  constructor() {}

  ngOnInit(): void {}
}
