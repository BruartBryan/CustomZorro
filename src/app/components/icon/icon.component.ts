import { Component, Input, OnInit } from '@angular/core';
import { IconPrefix } from '@fortawesome/free-regular-svg-icons';
import { IconName } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'cz-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent implements OnInit {
  @Input() color: string = 'black';
  @Input() iconStyle: IconPrefix = 'fas';
  @Input() iconName: IconName = 'coffee';
  @Input() size:
    | '2xs'
    | 'xs'
    | 'sm'
    | 'lg'
    | 'xl'
    | '2xl'
    | '1x'
    | '2x'
    | '3x'
    | '4x'
    | '5x'
    | '6x'
    | '7x'
    | '8x'
    | '9x'
    | '10x' = '2x';

  constructor() {}

  ngOnInit(): void {}
}
