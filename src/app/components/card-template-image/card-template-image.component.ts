import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cz-card-template-image',
  templateUrl: './card-template-image.component.html',
  styleUrls: ['./card-template-image.component.scss'],
})
export class CardTemplateImageComponent implements OnInit {
  @Input() title: string = 'Modeste demeure familiale';
  @Input() description: string =
    "Lorem Ipsum is simply dummy paragraph of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy paragraph ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sh";
  @Input() imgSource: string = './assets/ressources/img/maison1.png';

  constructor() {}

  ngOnInit(): void {}
}
