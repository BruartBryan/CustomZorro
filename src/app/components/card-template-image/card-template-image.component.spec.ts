import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTemplateImageComponent } from './card-template-image.component';

describe('CardTemplateImageComponent', () => {
  let component: CardTemplateImageComponent;
  let fixture: ComponentFixture<CardTemplateImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardTemplateImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTemplateImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
