import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cz-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() height: number = 400;
  @Input() width: number = 350;
  @Input() backgroundColor: string = '#f8f8f8';

  constructor() {}

  ngOnInit(): void {}
}
