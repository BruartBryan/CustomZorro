import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { fr_FR, NZ_I18N } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import fr from '@angular/common/locales/fr';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { ButtonComponent } from './components/button/button.component';
import { IconComponent } from './components/icon/icon.component';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import {
  faCoffee,
  faCouch,
  faPhone,
  faHeart,
  fas,
} from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { CardComponent } from './components/card/card.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { TitleComponent } from './components/title/title.component';
import { SubtitleComponent } from './components/subtitle/subtitle.component';
import { ParagraphComponent } from './components/paragraph/paragraph.component';
import { CardTemplateImageComponent } from './components/card-template-image/card-template-image.component';

registerLocaleData(fr);

const components = [
  ButtonComponent,
  IconComponent,
  CardComponent,
  AvatarComponent,
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ...components,
    TitleComponent,
    SubtitleComponent,
    ParagraphComponent,
    CardTemplateImageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzAvatarModule,
    NzButtonModule,
    FontAwesomeModule,
  ],
  exports: [...components],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (iconLibrary: FaIconLibrary) => {
        return async () => {
          iconLibrary.addIcons(faCoffee, faPhone, faCouch, faHeart);
        };
      },
      deps: [FaIconLibrary],
      multi: true,
    },
    { provide: NZ_I18N, useValue: fr_FR },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far);
  }
}
