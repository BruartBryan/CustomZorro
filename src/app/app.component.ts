import { Component } from '@angular/core';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'CustomZorro';
  clicked = false;
  loadingBuy = false;
  currentSlide = 0;

  constructor(private scroller: ViewportScroller) {}

  click() {
    console.log('click');
    this.clicked = !this.clicked;
  }

  scrollToNext(number: number) {
    this.currentSlide = number;
    switch (number) {
      case 1:
        this.scrollSmoothly('FirstSlide');
        break;
      case 2:
        this.scrollSmoothly('SecondSlide');
        break;
      case 3:
        this.scrollSmoothly('ThirdSlide');
        break;
      case 4:
        this.scrollSmoothly('FourthSlide');
        break;
      case 5:
        this.scrollSmoothly('FiveSlide');
        break;
      case 6:
        this.scrollSmoothly('SixthSlide');
        break;
      case 7:
        this.scrollSmoothly('SeventhSlide');
        break;
        case 8:
        this.scrollSmoothly('FinalSlide');
        break;
      default:
        this.scrollSmoothly('FirstSlide');
        break;
    }
  }

  scrollSmoothly(name: string) {
    document?.getElementById(name)?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  buyClick() {
    this.loadingBuy = true;
    setTimeout(() => {
      this.loadingBuy = false;
    }, 3000);
  }
}
