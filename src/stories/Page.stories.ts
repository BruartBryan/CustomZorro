import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import Page from './page.component';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { APP_INITIALIZER } from '@angular/core';
import {
  faCoffee,
  faPhone,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSwitchModule } from 'ng-zorro-antd/switch';

import Button from './components/button/button.component';
import Icon from './components/icon/icon.component';
import Avatar from './components/avatar/avatar.component';
import Header from './components/header/header.component';
import HeaderComponent from './components/header/header.component';
import Card from './components/card/card.component';
import { AvatarComponent } from '../app/components/avatar/avatar.component';
import { IconComponent } from '../app/components/icon/icon.component';
import { ButtonComponent } from '../app/components/button/button.component';
import { CardComponent } from '../app/components/card/card.component';
import { CardTemplateImageComponent } from '../app/components/card-template-image/card-template-image.component';
import { ParagraphComponent } from '../app/components/paragraph/paragraph.component';
import { TitleComponent } from '../app/components/title/title.component';
import { SubtitleComponent } from '../app/components/subtitle/subtitle.component';

// @ts-ignore
import headerIcon from './assets/res/img/header/iconHome.png';

const antDesignIcons = AllIcons as unknown as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(
  (key) => antDesignIcons[key]
);

let BBComponent = [
  AvatarComponent,
  IconComponent,
  ButtonComponent,
  CardComponent,
  HeaderComponent,
  CardTemplateImageComponent,
  ParagraphComponent,
  TitleComponent,
  SubtitleComponent,
];
let storybookComponent = [Button, Header, Card, Icon, Avatar];

export default {
  title: 'Example/Page',
  component: Page,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/angular/configure/story-layout
    layout: 'fullscreen',
  },
  decorators: [
    moduleMetadata({
      declarations: [...storybookComponent, ...BBComponent],
      imports: [
        CommonModule,
        NzAvatarModule,
        NzIconModule,
        FontAwesomeModule,
        NzButtonModule,
        NzSwitchModule,
      ],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => {
            return async () => {
              iconLibrary.addIcons(faCoffee, faPhone);
            };
          },
          deps: [FaIconLibrary],
          multi: true,
        },
        { provide: NZ_ICONS, useValue: icons },
      ],
      // schemas: [NO_ERRORS_SCHEMA],
    }),
  ],
} as Meta;

const Template: Story<Page> = (args: Page) => ({
  props: args,
});

export const LoggedOut = Template.bind({});
LoggedOut.args = {
  icon: headerIcon,
};

// More on interaction testing: https://storybook.js.org/docs/angular/writing-tests/interaction-testing
export const LoggedIn = Template.bind({});
LoggedIn.args = {
  user: {
    name: 'Andrew Brown',
  },
  icon: headerIcon,
};

// LoggedIn.play = async ({ canvasElement }) => {
//   const canvas = within(canvasElement);
//   const loginButton = await canvas.getByRole('button', { name: /Log in/i });
//   await userEvent.click(loginButton);
// };
