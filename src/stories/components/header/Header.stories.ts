import type { Meta, Story } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';

import Button from '../button/button.component';
import Header from './header.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ButtonComponent } from '../../../app/components/button/button.component';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { AvatarComponent } from '../../../app/components/avatar/avatar.component';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';

// @ts-ignore
import image from '../../assets/res/img/avatar/andrew.jpg';
// @ts-ignore
import icon from '../../assets/res/img/header/bmw.png';
import { ParagraphComponent } from '../../../app/components/paragraph/paragraph.component';

export default {
  title: 'Component/Header',
  component: Header,
  decorators: [
    moduleMetadata({
      declarations: [
        Button,
        ButtonComponent,
        AvatarComponent,
        ParagraphComponent,
      ],
      imports: [
        CommonModule,
        NzButtonModule,
        NzIconModule,
        FontAwesomeModule,
        NzSwitchModule,
        NzAvatarModule,
      ],
    }),
  ],
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/angular/configure/story-layout
    layout: 'fullscreen',
  },
} as Meta;

const Template: Story<Header> = (args: Header) => ({
  props: args,
});

export const LoggedIn = Template.bind({});
LoggedIn.args = {
  user: {
    name: 'John Doe',
  },
  avatarColor: '#1b1b93',
  headerColor: '#F0F1F5FF',
  iconSource: icon,
};

export const LoggedOut = Template.bind({});
LoggedOut.args = {
  iconSource: icon,
  buttonColor: '#1b1b93',
  headerColor: '#F0F1F5FF',
};
