import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../User';
// @ts-ignore
import image from '../../assets/res/img/avatar/andrew.jpg';
// @ts-ignore
import icon from '../../assets/res/img/header/bmw.png';

@Component({
  selector: 'storybook-header',
  template: ` <header>
    <div class="wrapper" [style]="{ backgroundColor: headerColor }">
      <img [src]="iconSource" style="height: 60px" />
      <div style="display: flex; flex-direction: row">
        <div
          style="display: flex; justify-content: center; align-items: center; rotate: 90deg; padding-top: 12px"
        ></div>
        <div *ngIf="user">
          <cz-avatar
            [customSize]="64"
            [imgSrc]="imgSource"
            [displayName]="'John Doe'"
            [customStyle]="{ backgroundColor: avatarColor }"
          ></cz-avatar>
        </div>
        <div
          *ngIf="!user"
          style="    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;"
        >
          <storybook-button
            *ngIf="!user"
            size="small"
            class="margin-left"
            (onButtonClick)="onLogin.emit($event)"
            [label]="'Sign in'"
            [color]="buttonColor"
            [backgroundColor]="buttonColor"
            [outLined]="true"
          ></storybook-button>
          <div style="height: 8px"></div>
          <storybook-button
            *ngIf="!user"
            primary
            size="small"
            primary="true"
            class="margin-left"
            (onButtonClick)="test($event)"
            [label]="'Sign up'"
            [color]="'#fff'"
            [backgroundColor]="buttonColor"
            [borderRadius]="120"
          ></storybook-button>
        </div>
      </div>
    </div>
  </header>`,
  styleUrls: ['./header.css'],
})
export default class HeaderComponent {
  @Input()
  user: User | null = null;

  @Input()
  imgSource: string = image;

  @Input()
  iconSource = icon;

  @Input()
  customStyle: any = { background: 'red' };

  @Input()
  avatarColor = 'white';

  @Input()
  buttonColor = 'black';

  @Input()
  headerColor = 'white';

  @Output()
  onLogin = new EventEmitter<string>();

  @Output()
  onLogout = new EventEmitter<string>();

  @Output()
  onCreateAccount = new EventEmitter<string>();

  test(event: any) {
    console.log('Click from header');
    this.onCreateAccount.emit(event);
  }
}
