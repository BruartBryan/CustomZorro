import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IconName } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'storybook-button',
  template: ` <cz-button
    (clickButton)="this.click($event)"
    [size]="size"
    [backgroundColor]="backgroundColor"
    [color]="color"
    [borderRadius]="borderRadius"
    [loading]="loading"
    [linearGradient]="linearGradient"
    [outLined]="outLined"
  >
    <div *ngIf="icon && iconColor" style="padding-left: 8px; padding-right:8px">
      <cz-icon
        [color]="iconColor"
        [iconName]="icon"
        [size]="iconSize"
      ></cz-icon>
    </div>
    <cz-paragraph [bold]="labelBold">
      {{ label }}
    </cz-paragraph>
  </cz-button>`,
  styleUrls: ['./button.css'],
})
export default class ButtonComponent {
  /**
   * What background color to use
   */
  @Input()
  backgroundColor?: string;

  /**
   * What color to use
   */
  @Input()
  color?: string;

  /**
   * What border radius to use
   */
  @Input()
  borderRadius: number = 8;

  /**
   * What loading to use
   */
  @Input()
  loading: boolean = false;

  /**
   * What outLined to use
   */
  @Input()
  outLined: boolean = false;

  /**
   * What loading to use
   */
  @Input()
  linearGradient?: string;

  /**
   * How large should the button be?
   */
  @Input()
  size: 'small' | 'medium' | 'large' = 'medium';

  @Input()
  iconSize: string = 'sm';

  /**
   * Button contents
   *
   * @required
   */
  @Input()
  label = 'Click Me';

  /**
   *   icon content
   *
   * @required
   */
  @Input()
  icon?: IconName;

  /**
   *   icon content
   *
   * @required
   */
  @Input()
  labelBold = true;

  /**
   *   iconColor content
   *
   * @required
   */
  @Input()
  iconColor?: string;

  /**
   * Optional click handler
   */
  @Output()
  onButtonClick = new EventEmitter<string>();

  click($event: any) {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 3000);
    this.onButtonClick.emit($event);
  }
}
