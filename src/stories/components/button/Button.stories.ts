// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/angular/types-6-0';
import Button from './button.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from '../../../app/components/button/button.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { APP_INITIALIZER } from '@angular/core';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { faCoffee, faPhone } from '@fortawesome/free-solid-svg-icons';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { IconComponent } from '../../../app/components/icon/icon.component';
import { ParagraphComponent } from '../../../app/components/paragraph/paragraph.component';

export default {
  title: 'Component/Button',
  component: Button,
  parameters: {
    docs: {
      description: {
        component: 'Définition de cz-button',
      },
    },
  },
  decorators: [
    moduleMetadata({
      declarations: [ButtonComponent, IconComponent, ParagraphComponent],
      imports: [CommonModule, NzButtonModule, NzIconModule, FontAwesomeModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => {
            return async () => {
              iconLibrary.addIcons(faCoffee, faPhone);
            };
          },
          deps: [FaIconLibrary],
          multi: true,
        },
      ],
    }),
  ],
  // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
    color: { control: 'color' },
    borderRadius: { control: 'number' },
    loading: { control: 'boolean' },
    outLined: { control: 'boolean' },
    linearGradient: { control: 'text' },
    iconColor: { control: 'color' },
    icon: { control: 'select', options: ['phone', 'coffee'] },
    iconSize: {
      control: 'select',
      options: [
        '2xs',
        'xs',
        'sm',
        'lg',
        'xl',
        '2xl',
        '1x',
        '2x',
        '4x',
        '6x',
        '8x',
        '10x',
      ],
    },
  },
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<Button> = (args: Button) => ({
  props: args,
});

export const Basic = Template.bind({});
Basic.args = {
  size: 'large',
  label: 'Button',
  backgroundColor: '#000',
  color: '#fff',
};

export const Gradiant = Template.bind({});
Gradiant.args = {
  size: 'small',
  label: 'Gradiant',
  linearGradient: 'linear-gradient(0.25turn,#e66465, #9198e5)',
  color: '#fff',
};

export const OutLined = Template.bind({});
OutLined.args = {
  size: 'large',
  label: 'Button',
  backgroundColor: '#000',
  color: '#000',
  outLined: true,
};

export const OutLinedGradiant = Template.bind({});
OutLinedGradiant.args = {
  size: 'small',
  label: 'Gradiant Outlined',
  linearGradient: 'linear-gradient(0.25turn,#e66465, #9198e5)',
  color: '#000',
  outLined: true,
};

export const IconButton = Template.bind({});
IconButton.args = {
  size: 'large',
  label: 'Drink',
  backgroundColor: '#81591e',
  color: '#81591e',
  icon: 'coffee',
  iconColor: '#81591e',
  outLined: true,
  iconSize: 'sm',
};
