import { Component, Input } from '@angular/core';

@Component({
  selector: 'storybook-icon',
  template: ` <div>
    <cz-icon [color]="color" [iconName]="icon" [size]="size"></cz-icon>
  </div>`,
  styleUrls: ['./icon.css'],
})
export default class IconComponent {
  /**
   * What colors to use
   */
  @Input()
  color?: string;

  /**
   * How large should the icon be?
   */
  @Input()
  size: string = '2x';

  /**
   * icon contents
   *
   * @required
   */
  @Input()
  icon = 'icon';
}
