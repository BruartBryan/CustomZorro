// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/angular/types-6-0';
import icon from './icon.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { IconComponent } from '../../../app/components/icon/icon.component';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { APP_INITIALIZER } from '@angular/core';
import { faCoffee, faPhone } from '@fortawesome/free-solid-svg-icons';

// More on default export: https://storybook.js.org/docs/angular/writing-stories/introduction#default-export
export default {
  title: 'Component/Icon',
  component: icon,
  decorators: [
    moduleMetadata({
      declarations: [IconComponent],
      imports: [CommonModule, FontAwesomeModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => {
            return async () => {
              iconLibrary.addIcons(faCoffee, faPhone);
            };
          },
          deps: [FaIconLibrary],
          multi: true,
        },
      ],
    }),
  ],
  // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
  argTypes: {
    color: { control: 'color' },
    icon: { control: 'select', options: ['phone', 'coffee'] },
    size: {
      control: 'select',
      options: [
        '2xs',
        'xs',
        'sm',
        'lg',
        'xl',
        '2xl',
        '1x',
        '2x',
        '4x',
        '6x',
        '8x',
        '10x',
      ],
    },
  },
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<icon> = (args: icon) => ({
  props: args,
});

export const phone = Template.bind({});
phone.args = {
  size: '2x',
  icon: 'phone',
  color: '#FFC0CB',
};

export const coffee = Template.bind({});
coffee.args = {
  size: '2x',
  icon: 'coffee',
  color: '#ab5905',
};
