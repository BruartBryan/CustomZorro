import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'storybook-card',
  template: ` <cz-card
    [backgroundColor]="backgroundColor"
    [height]="cardHeight"
    [width]="cardWidth"
  >
    <ng-container *ngIf="isTemplated">
      <ng-container *ngIf="!customTemplate; else custom">
        <cz-card-template-image
          [description]="description"
          [title]="title"
          [imgSource]="imgSource"
        ></cz-card-template-image>
      </ng-container>
      <ng-template #custom>
        <div style="    display: flex; flex: 1;flex-direction: column">
          <div style=" display: flex; justify-content: space-between">
            <div style="margin-left: 10px; margin-top: 5px">
              <cz-title>Colombian Coffee</cz-title>
            </div>
            <div style="margin: 10px">
              <cz-icon color="#776402"></cz-icon>
            </div>
          </div>
          <div style="display: flex; justify-content: center">
            <img src="assets/ressources/img/juan.png" style="width: 520px" />
          </div>
          <div style="margin:10px ; display: flex; flex: 1">
            <div style="display: flex; justify-content: space-between; flex: 1">
              <div>
                <cz-subtitle [underlined]="true">Special delivery</cz-subtitle>
                <div style="display: flex; flex-direction: row; gap:0.5rem">
                  <div>
                    <cz-paragraph
                      style="color: red; font-size: 18px;  text-decoration: line-through;"
                      >50€
                    </cz-paragraph>
                  </div>
                  <div style="display: flex; flex-direction: row; ">
                    <cz-paragraph
                      [bold]="true"
                      style="color: green; font-size: 18px;"
                      >49€
                    </cz-paragraph>
                    <cz-paragraph [bold]="true" style=" font-size: 18px;"
                      >/g
                    </cz-paragraph>
                  </div>
                </div>
              </div>
              <div style="display: flex; align-self: center">
                <storybook-button
                  (onButtonClick)="onTest($event)"
                  [backgroundColor]="'#776402'"
                  [borderRadius]="8"
                  [color]="'#F8F8F8'"
                  [loading]="loadingBuy"
                  [label]="'Order'"
                >
                </storybook-button>
              </div>
            </div>
          </div>
        </div>
      </ng-template>
    </ng-container>
  </cz-card>`,
  styleUrls: ['./card.css'],
})
// /**
//  * Is this the principal call to action on the page?
//  */
// @Input()
// primary = false;
export default class CardComponent {
  loadingBuy = false;

  /**
   * What background color to use
   */
  @Input()
  backgroundColor?: string;

  /**
   * How large should the icon be?
   */
  @Input()
  size: 'small' | 'medium' | 'large' = 'medium';

  /**
   * icon contents
   *
   * @required
   */
  @Input()
  label = 'card';

  @Input()
  isTemplated = false;

  @Input()
  customTemplate = false;

  @Input()
  title = 'title';

  @Input()
  cardHeight = 400;

  @Input()
  cardWidth = 300;

  @Input()
  description = 'description';

  @Input()
  imgSource = '';

  @Output()
  onClickCardButton = new EventEmitter<string>();

  onTest(event: any) {
    console.log('Click from storybook-card');
    this.onClickCardButton.emit(event);
    this.loadingBuy = true;
    setTimeout(() => {
      this.loadingBuy = false;
    }, 10000);
  }
}
