// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/angular/types-6-0';
import card from './card.component';
import { CardComponent } from '../../../app/components/card/card.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { CardTemplateImageComponent } from '../../../app/components/card-template-image/card-template-image.component';
import { ParagraphComponent } from '../../../app/components/paragraph/paragraph.component';
import { TitleComponent } from '../../../app/components/title/title.component';
import { SubtitleComponent } from '../../../app/components/subtitle/subtitle.component';
import Button from '../button/button.component';
import { ButtonComponent } from '../../../app/components/button/button.component';
import { IconComponent } from '../../../app/components/icon/icon.component';
import { APP_INITIALIZER } from '@angular/core';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { faCoffee, faPhone } from '@fortawesome/free-solid-svg-icons';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';

// More on default export: https://storybook.js.org/docs/angular/writing-stories/introduction#default-export
export default {
  title: 'Component/Card',
  component: card,
  decorators: [
    moduleMetadata({
      declarations: [
        CardComponent,
        CardTemplateImageComponent,
        ParagraphComponent,
        TitleComponent,
        SubtitleComponent,
        ButtonComponent,
        IconComponent,
        Button,
        ButtonComponent,
      ],
      imports: [CommonModule, FontAwesomeModule, NzButtonModule, NzIconModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => {
            return async () => {
              iconLibrary.addIcons(faCoffee, faPhone);
            };
          },
          deps: [FaIconLibrary],
          multi: true,
        },
      ],
    }),
  ],
  // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
    isTemplated: { control: 'boolean' },
    customTemplate: { control: 'boolean' },
    title: { control: 'text' },
    description: { control: 'text' },
    imgSource: {
      control: 'select',
      options: [
        '../../assets/ressources/img/hotel.jpg',
        '../../assets/ressources/img/cabane.png',
        '../../assets/ressources/img/hutte.png',
      ],
    },
    cardWidth: { control: { type: 'range', min: 300, max: 1000, step: 8 } },
    cardHeight: { control: { type: 'range', min: 300, max: 1000, step: 8 } },
  },
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<card> = (args: card) => ({
  props: args,
});

export const Empty = Template.bind({});
Empty.args = {
  backgroundColor: '#fff',
  isTemplated: false,
  customTemplate: false,
  cardHeight: 450,
  cardWidth: 400,
};

export const Empty2 = Template.bind({});
Empty2.args = {
  backgroundColor: '#fff',
  isTemplated: false,
  customTemplate: false,
  cardHeight: 100,
  cardWidth: 400,
};

export const Template_ONE = Template.bind({});
Template_ONE.args = {
  isTemplated: true,
  backgroundColor: '#fff',
  customTemplate: false,
  title: 'Hôtel 5 étoiles',
  description: "Petits fours & douche offerts à l'arrivée",
  imgSource: '../../assets/ressources/img/hotel.jpg',
  cardHeight: 400,
  cardWidth: 400,
};

export const Template_Custom = Template.bind({});
Template_Custom.args = {
  isTemplated: true,
  backgroundColor: '#fff',
  customTemplate: true,
  cardHeight: 450,
  cardWidth: 400,
  title: 'Colombian Coffee',
  description: undefined,
};
