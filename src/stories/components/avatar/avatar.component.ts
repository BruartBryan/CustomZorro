import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'storybook-avatar',
  template: ` <cz-avatar
    [customSize]="size"
    [customStyle]="{
      color: color,
      backgroundColor: backgroundColor,
      fontWeight: isBold ? 'bold' : 'normal'
    }"
    [imgSrc]="imageSource"
    [displayName]="label"
    [shape]="shape"
    [nzIcon]="icon"
  ></cz-avatar>`,
  styleUrls: ['./avatar.css'],
})
export default class AvatarComponent {
  /**
   * What background color to use
   */
  @Input()
  backgroundColor?: string;

  /**
   * What colors to use
   */
  @Input()
  color?: string;

  /**
   * What image source to use
   */
  @Input()
  imageSource?: string;

  /**
   * What shapes to use
   */
  @Input()
  shape?: 'square' | 'circle';

  /**
   * How large should the button be?
   */
  @Input()
  size: number = 50;

  /**
   * How large should the button be?
   */
  @Input()
  icon: string = '';

  /**
   * is bold ?
   */
  @Input()
  isBold: boolean = false;

  /**
   * Button contents
   *
   * @required
   */
  @Input()
  label = 'avatar';

  /**
   * Optional click handler
   */
  @Output()
  onClick = new EventEmitter<Event>();
}
