// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/angular/types-6-0';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import avatar from './avatar.component';
import { AvatarComponent } from '../../../app/components/avatar/avatar.component';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { APP_INITIALIZER } from '@angular/core';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import {
  faCoffee,
  faPhone,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';
import { IconComponent } from '../../../app/components/icon/icon.component';
import * as AllIcons from '@ant-design/icons-angular/icons';
// @ts-ignore
import image from '../../assets/res/img/avatar/avatar2.png';

const antDesignIcons = AllIcons as unknown as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(
  (key) => antDesignIcons[key]
);

// More on default export: https://storybook.js.org/docs/angular/writing-stories/introduction#default-export
export default {
  title: 'Component/Avatar',
  component: avatar,
  parameters: {
    docs: {
      description: {
        component: 'Hello everybody! I\'m the documentation !'
      },
    },
  },
  decorators: [
    moduleMetadata({
      declarations: [AvatarComponent, IconComponent],
      imports: [CommonModule, NzAvatarModule, NzIconModule, FontAwesomeModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => {
            return async () => {
              iconLibrary.addIcons(faCoffee, faPhone);
            };
          },
          deps: [FaIconLibrary],
          multi: true,
        },
        { provide: NZ_ICONS, useValue: icons },
      ],
      // schemas: [NO_ERRORS_SCHEMA],
    }),
  ],
  // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' },
    color: { control: 'color' },
    imageSource: { control: 'text' },
    shape: { control: 'select', options: ['square', 'circle'] },
    size: { control: { type: 'range', min: 10, max: 250, step: 10 } },
    label: { control: 'text' },
    icon: { control: 'text' },
    isBold: { control: 'boolean' },
  },
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<avatar> = (args: avatar) => ({
  props: args,
});

export const Basic = Template.bind({});
Basic.args = {
  size: 50,
  label: 'BB',
  isBold: true,
  backgroundColor: '#000',
  color: '#fff',
};

export const Square = Template.bind({});
Square.args = {
  size: 50,
  label: 'BB',
  isBold: true,
  shape: 'square',
  backgroundColor: '#000',
  color: '#fff',
};

export const Icon = Template.bind({});
Icon.args = {
  size: 80,
  label: '',
  backgroundColor: '#000',
  color: '#fff',
  icon: 'user',
};

export const Avatar = Template.bind({});
Avatar.args = {
  size: 150,
  label: '',
  backgroundColor: '',
  color: '',
  imageSource:
    'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYVFRgVFRYYGBgaGBgaGhgYGBgYGBocGBkZGRgYGBgcIS4lHB4rHxgaJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHjQrJCU0NDQ0NDQ0NDQ0NDQ0NDQ0NDE0NDQ2NDQ0NDQ0NDQ0NDQ0PzQ0NDQxNDQ0NDQ0NDQ0NP/AABEIAM8A9AMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAAECBwj/xAA7EAABAwMCAwYEAwgBBQEAAAABAAIRAwQhEjEFQVEGImFxgZEyobHBUtHwExQjQmKCkuHxB3KissIk/8QAGQEAAwEBAQAAAAAAAAAAAAAAAQIDBAAF/8QAJhEAAwACAgIBBAIDAAAAAAAAAAECESEDMRJBBBMiMmFCUSNxgf/aAAwDAQACEQMRAD8A8xWBYthMSJ6AXdYrKK5rok/5A62FsBaK4Yxy5C2umtXBZ0wKZoUQU9IhsFwkZgSATCDeAKfJ4Br55BDANviJ2J6LmGDkPKSFlwyTgSeurnzUdC1dv49Eqh0Xyo0E1AWAAtaW7gHPlPVCXLAYcJGqZEYHkmjLZ4bqAydzH2MrioHOGksA8h13+af6VL0B80NdgjW6WTzP6lR06x0n4CRyLZJ8zyUl5SPwjPl4KE2zhkiPRNctvSFiku2Ql8z3QB06eLTuFExxaZUz6Z5lRBnh6qeMFMm6jw4yQZ6yFw6niQZ8Oi08Fc6kDiRlUjHspYDj3TvyOPNQB62x2khw6+fouOJNDm5HLkjrSuHDfPNCvfq7w9QoxjvNXZBUprA8aNkZSalthX1gdU2YMKkswcqcvBBcKBjsqa4KFYMosMrQcCprYZQ1NF24SoSloYsdhYog5YmE8SnlSsZK5oMLiAFceF9mSWhzglUt9HoalbKyxsLl7ZVvuez4bySW54YQcKn0aJ+c5ErmqIo64tXN5IMqblrsbT2jbGrohY1y1K4XZJTaCc/kjuFcPfdPDG4YN9wAPzQ1vZuqOYxklz3BrQPHc+QXq3ZjgbLZoaBkAlx5knmU3HPlWX0hrrwn9sq9Dso0P0ux4DomFfgjAQ1rYHmrEyHVT7KarRbr/XI8wt84k86nVeyv1eCMawACCgbrhw/PHRXmsxobO7RvynlHgkly1pBgAmJnkAN8bc1yrIfDGynM4NrdHMnl5pfxHh3eLQNsK7FgYyTnlsgWWjcud/bGZ3zK7TDloo1ThGnLggKtvyG31Vz4kWwWjc/LxSN1CMbqVcaLTyv2V2pbjkhnsI3VlHDsycAoK+tA0eCz1xPs0Typ6EhCymcwtvYuNKgzQHOOhxHPB8FHWADjG28ea1uBPv5Fd1nAgeZHpGEAm6DtJDh1yrLTfLZCqFN+k+HMKx8Kq93RvjB+iaXhmfmnM5/olqqFoypKyiaU7ZCegymi6IQdIo2kgib7J5WLNKxPgfAx7LdlyX63DE4XoFe3bSYjOE02MYNkj7S8UbJAKMP0X5MCPid8ASkTroOKgvqpJS81YK2LSMr2WZ1i17NlVXW5p1wBGZAkAjPgVaOEXUsSHjsio1w5EH2MqHJs0cbwS3NrSdirTNJ3OpT+Hzcw49ko4lwp9Bw1Q5pALXtyxwOxB5L0utYMqU2PAw5rT7gIB3Dv4FSn+DvsnxwW+U59VnSecM01C8fJA/8A004OH1HXDhIZDGz1jU//AOVerh4a8gYJQHY22FGzp4y7U8/3mfpCk4gTq1D9eC08M6MXPQBUBbUkRvv1nCcgSC7c4zzjM/qEBQc1+CII3np4FHW4LO6Qen3hWozcaeTdYANdJBBH5FJWDUYERMR18/JF8VqEAhu5+2NkBa0TMEncExgxzyguijOngO1blvInHMSQBy8fFJuI1tIgEExygRGPVMrmhoJaI2jqfOClNa23c7f9ckUIJNLnH7rX7MBEXNYDASyq9zlz2FHVSoAEtruLt9kQ6i474WxQA3UqZaRJXtZ5IHTByrDckRASS6Cz3K7NPHXo1Rdg8x+a4btnnstWzDlbfh2noT9VAsQO3TPgtxDtPVAig5zoAk/TzRlGi2mQXHU78LcD1KbHsD3od1wh2MO5B9lF+9VH7HT4NH3RFtauOSXHxJK52s4En4tKdhNAJhTCCtgnNnbTkp5MWPuOWMwsTJlphYnyVLPY3bjTAHRVKvQfUqOBndMey/EQ9gE5TdlNgcSjKxsemVPiHDtDZVWuZlX3tBcNiAqZcNCq23OiUpKthHCrktwi73S7JSthDRKAuuImYCSqwtlZSb0ek9nuIB1uG7/s+64cwJOk+yNDZJaMhzHjzkSPI4XnfZK/LLmmCe5UcKbxyh+AfMGF6VbW414wWZjkeimtsuq+xpht67QxjGkDSwCAc4AC1aPDu67oOk5HRIeJ8Oe8ueXiSZ5pa2rWpkFrg6DPt57rclrB5VNussvL7UNyPQ+PRdW79eDAI5zCS2HFXvZq3zDmnl4gJmZDgeo6g7iVN59lZx6Ib4Aub4DK1bUxyyT9uULm8J1bR3R/zlbc8MYGg/Ecz+e/NHeDtA9y8Fx0iXTkgQPYJdcWrnd4uOBGMegKY3F2ymAOqT8R4+1g0mDvAjHTV5orQrx6Aru1Y3On13Sqqz8LVp/H2CdRJnpMevXKFr8fa44bC51Jymv6MqMd0QppmclTO4k13ULn9kXZBkJHhlJTXZA6m0JLxBokqzOsoAJ5/ZIeLNA2Ub6Lxpiu3cQcdFjz358ZWU8OC7qMjJ/EszNKJm1TB5TuVqhTkrsN1eqMtqYQfRSVsYWVqIkx6I2u9rGGN3YHqh6TgBJwBugq1Yvd4DYITONsPPzJT4rsacGt9bw1eiWnDW6YACoHZ6roqA+i9Q4Y8OhM6wzzoSyD0uHNjZbTwWhOYWI5ZTCPE+zd3p5q1m6dG6874HVOqPFXG4qgMk9Fonck6WKFXFr8l0fdLBVkrLl0kqAN5optBaR3dXEBKWmTKluqmVHQ3UqrLLTOEMLOoWPY8fyOa7/Eg/Zetca4y2gx9RokODdMQCdWZHXf6ryVmyvT7N9aypQ/AYym0OJjXreIPgAW+kLk8bAvueBG3tFVe7LKhDsAMiZJ2yDJnkpKnEHseWuD2uHxU6zCx48pwVdW8Nax1B1Nrf4VOk0AiRqa0OJPXvElU274Je3XEHVK1OGucC52GtLAIAAmZiFKfmPyafotfxpwsex/wm7/AJmGA4EH1wR81a6DtQGIjYDbyzyVRs+HPtq5pPEtc3Ww+E5x1GVc7B2kgg+GN+W4W+bVyqR5tS4tywO+ZL4MjYePL7QhazpfBJwNLZG5GzZ2HJML53enok/FLpzu6ACckn+bAOokk890TmJOJ3kuc9wnBAAMAchHWOiqd6/U6XZ+ya8ZugXGAGg7ATA8J/W6qtxWLjHLmp3ZTijOwnWznE9AiKbaR5BcVbJ9K3bcF7GNLtAY0tNU9XOB2CkseHVK7A+m/VuCHtGCP5ZG26g+eZWX0afoU9HFS2b/ACqfhVUteAdiYIS6q59J5ZUbpPuPQou27xnmnVqllCOHLwy6cTs4Y1zRyXnvEviPmvV6RD7YE7hhJ9AvI+IGXu81zeg4AKeXLq5OY8VuiRnqFphGpsrO+y66DWMj9eCPtKfN0oZrsFx6yeiHrcSwWt3P83TrHj4o4CqwM6l214c1uzSM9d/koqJyhOHtim7/ALmx85RVDdcjPyreSxcEt9Txhevdn+HQ0ErzXshTBeCeS9k4ZGgQkp7E4pyEspACFikWJjRhHytwBsv9VbuIUTowq12ZoHXPirjxF4DI8FplfaZOV/cUZ86kQ0d1ZXb3lMynLU6Qclfu/iW7dSX7IKjt91mpbNCeZGFNendkLDXw9xc4j9pWeR0a1gDBHq0nzK8xBwvYOxrgLCgzkNZPq90p5SdYFw1LoV2fE30XaajdYBw4QCRy1NOHGIyCEc7ttTaR/C+gR13wlj8vmCCBBzpnAS5/A6TQIYCerp36CTlGvi8dVloE/IuVtgH73WuLkVXaRT/ZuDQ0yQTHxcwdlcbUTAjOBIjcfrZIba3DHEwAYjAjnCeWLhk9P1gK88aiPFGa+R3yeTFvEnQTMY9sdQqtxC4DhjuxqknZ0RDW4wR5pzxi4jUGk56Hlv8AVU+/uCZJPXbqTk+KLB2xTxavMxsNueUpsnN/aM1fDqyfE7I27ZJgLuwoHwbymM+6z8kOtGniqZ2yzXPZhldgyd5DmxIkcyforFwfglO0pgRAEnvQS48y72CptOpXYMOeQBJIPL18whbniFV+9R8HlJx7LHXw7x4t6NS+TOc42E9pNL5JgS6RjIHKBvlJLYOaQiadqSZz5nJ9yp208xC1cfF4Ijycvmy8cKfroiB/IZ+/0Xlt4Brd0k+y9X4TR0WznHkxx/8AErx+5dJefEgeqanhHY6I6bZl2wM/Nc0CC4frddVe6wDrJ+yjtBknwKzlDd1cufjZoOAPqepUACxbhE4a2Ihhzgn6Iu1bJCFtGfwwepTXglLVUaD1R9Eb7Lp2YtHtIcB0XrPDcNAVZ7P2Y0tgK30WaQp9vIYnBMsXOoLE2Sh4RwzhoZlR8VqSICbvqABILtpJkLcYMZ2xXVpRlZSq8lLcg7KGnQXLL6G0uwS9oakAyiQU+ezwUTrddXDlBnlwKbswB7/Jen9k7w/ubNJGHvGemHf/AEvNuJN0td5D5q0diL8fs30id9NQeg0Oj2asnFX+Xf7R6VcePjte2kz0u3uA5oIyeqjrNAzueqQ216WQPHqmNS9Dm7xhb/HDPJdZWCF1QueY/p2xzJTm2fDHE77mdyT9Uhsyck7zt5JqakUz7JqWUTj8slU4zW1E778h7qr3zzICs3EtiqxetEEykpaKz2BUXSU2txBluDnnsDvCQWdTvGfGPP8AJPbd4hJO0UpYY4YwEdcAbch9FHVptEkDcbmN+fpuov34NEk+HtsgbriuMbpmgIkvavU7CB6IWyBe8AZS2tWLirB2PtJqhx2UqorMlt7QvFCwfOCWR/liPmvG6rAWtzlzifQY+59l6P8A9UL8CkykDlx1HPIbBebXA0vgHYAepbJ+ZKhT0WXZBcOknwUlrhrusH5IZxwiWYYY35+SmMgY7LsjK46KQj3ROHNKdDARBz9U54EzvtPiEvsqEsZJnB+uE24UyHj0QfRG+z2Ls7V7o8laVSezzzAVzpmQEqHnoBuQ7UViMeRKxNhByeQutlpliDumbWLHshekeaVm/tADsgP2Kf37UsaAjPY1PQO20ldG0TCixTPpQ0noJTVWE2LK8mkUDtE7l/V9MfZC8F4gaVVhnAcQfJ2D+fos4xU1O9T8gAPqUBQZLgP1nC8mdbffZ7XLXk8Lrr/h6ux+rB3CLouIxz/QQ9xbaHjSZaQI9gmFhS1Ek8l6XHaqVS6Z4/LLmnIPdMcwa2ZPNvIpdd9oHBugkt8IViuKfVVzj/CWvAImRzal5E2sy9j8FY1Syit8S4xP85Pglbrpz9p9U0q8H05yT4hQfsAFHFfyZd1P8UDUKMZRzn48ekeAzK40rhyedEnt5NPqSh3uXVRyFfUS1RSZJ2PkwrZwauGRmAB+X69VULPeSi7i+wQ3CUdHXaK/Fzcj8Lev4W5J+SrdR+qSeZn3yii+GvPN3dHl/N8kGW7KVPZSSJ26KdhnsEMN0XcEaAB1HyCQYF5hSObn2XAOVIT3lxxd+z1uH0h6/VWbg/BxrBckHZ58UgY6fSVd+DVmkBKJSWSyWdroAhNxdaGoC3qh0AKa/p93C5dhFl5xyHELFWuIMOsrFb6RPzJtlFVesfchLL285Lb5GRRVPCA+KXMBKaF1ldcQ1EEpRRqd5I208mqeFeOGWincgLm64kGscZ2aUlq14CCfXL2uA5kNA6l2ybltKWRiH54FV3Sy3/sLj/cZA9oQFN0T5D6hNbwaRUzsQwHrG/0CUAz5yF5x6Cel/s9YtL8OcKTtxRpvH92oH20j3TBl22kwl20qi3nEP2d/TMw006bHeAcN/QkFXag4a26gIGc81f4T/wAKX6M3zFjlb/Ylu+1LHklzobyGyW3PH2O+DbqrrWrUT3SxpbnED2SW64RbPOGBpP4cH3CevLPY0KcdFbdxBjh8WfNC1LgdZHVOb3s7bgQC7y1H81X7jhjG/A53uk8qXYXEslbWB5rHu6IBtrB+M+SmaYTJv2SqUjVVDlile9RFCmckda+SHrPxC3UeAEG50pcjox759Fg5noFzTbJW6mG+Z+im3soujik2SibkQGgeJ+yHobqe9w4AbAIB9EDN12BlcsGVJRA1Ceq449I7MUZpO1coj/EbK18JtiGyql2fquayD+KPkCr/AMEGpqViV+QZYPIOUzuqkj0RFtw8aULxGmWA+SAyRUrxpLiVi3VLiZAMLFf6gnghBbPLkNxYaIJW7G5AKW9obonAVvLWQwpm2gc37XCAULRoy+Ql1AQ5WThzBAKeadHclYQv4nT0tQFlIAPi5w/tGD7p1xsCEsLGikXFxEdwCMba3Ok+MBS+Q8IT4+abEd8fuT6nf2CBYfqpK9XUXFRkQPMn5LOjQ9YGfag//od4NZH+DVeOyvEP3iiJPfZ3XdTGzvZUPtC6a5PVlP8A9GqXszxU29drie47uvH9J5+hTfHrxSE+RPk2Xni/DqhgscQR0Sk8Pu297WfUK8s0kTjz6ytXDgWxMjb/AEt1QnsxK6Wjzm5r3Aw6Cl76z+eFcuKMDQTAMg7jr0VarMEqNRgtNNoCFRcvKmeAhqjkuMBxk5LlDUqwualVDOJKTIyRj3ytErelY4IDmUVlwdh4fVd24xPmoKjpKQYmtB3s7BG8SYA1h05JPe6johbRk+X36Ini1Mt0AgjfnI5bJW9oeV9rYAwZKIsmy8c8xHmoG7onhrJeIMc/ZFiIvPC62mln8Zj0MFXXs5dZE7Kp8FsdenGMkepV6tuFw0EBKybT8i8WrwWiEJxilqZhR8JrY0nwRl28REoeik7Flvw1ukYWIxl02N1tdkbxPniyvCHKa+rByEfS0od7yt7WFgyTWayRl0OTKheaQlLnLthSy8Bt5GFxc6yBO5UXG6pFJjNeNGvTEQXmXN8TDW58VFRHeHtJ2E41HwCH7RVdVQgO1NB0gnmG90O8JhR53mki3x0plsRDYqasO6zyP1UTInKJuGRoHUfdIN2ScTpuc8uAkBrATywxoS9Om/A8nV8eCPhwEmduUsv1/Q/JON/2XLs/2limKTz3miGuPMDYeYTF3HJ5/NUfhbNVVg/q+mVcRwym4GQWu6tJHuNlu4nVSYOXxiv9gtzxOec9EtfdSj63Axye75IR3CWjcuPngfJLXl7DNTjQDUuOiGe4lM3WYCGfTU2mUVIB0rYYiSxclqXA3kQFqhqFEVENUQfQyJG/DPghkUB3PQ/JDNUxw60Gx6GVu/rtdpgkxMg8vJSUmhrJPTZB13hzpaIGMfVDGxs4nBpg7yN4QwF+eUn2CDaO8mHDAZcOoMIsVHpvZmoO4DnuheiUardPovNeBsIY13hv6lOqnFHNEBWjgdLJCuZJtFiq8VazYqv8W7XaQYPzVe4txBzgcqo3D3OcmfBKBPK2X6n2qdH+1ip1OYWkfoIH16N3Y3QDmSmFycoFxV2QkHdTXTBCkcFE5KPnJPbAkmNufkMk+wSS8fJJHL9Ep3bB2h8c4b/l/wAJHeiCdPwkkDy/5BWS/wAjXH4ogYwGBzcRHlKKrvBew8gPoSoKbgHAxGkH/X1WViQ1p6tj5ulIME06s0nAOMySWxLYPMpWpW1nBpaDAdEjrGyjXJYyNVeWBz2co/xmOOxLh66frlX0W0AEKhdn6bnuc1p2Ad6g4Pgrxf1alsym6sAWvgB7TifFaeLlmEpp4y9fsw/I43VZneFs6fRkIStbdU4tiHtBHNRXjAAtFLJnl40Vi5pgbJXcMTi7cJS6soUaJF7mLT2IrRK1Wj5JcD5F9RqGIRFdyiayfaVOi0o1p7pPmoGDbzU8ktJ5Z9yo7f4gpjhVwS1vmg3Iq6dgD+qf9IWpyROZKwd4BMLWWzH4wP8Ayz9EAz4mo+m2XBu0vz7brgo9H4JWDqLPCR6gmVJdvwlXCK0MA2yT80xuDhehxv7Ezz+T8mhNctJS8W3ehWFlvIXNO0zMJlOdiO8LBBb2vdCxN2W5hYnwSyUqq+SoXOXGpcuKg2aUjZeo3FbC0/ZLTGSC6btNIu5APefkxnzLvZV97DqAnlPsJ+6dcVaWsYwbOc1p8wJPpLyUoqmHPI+fjIWNvLbNiWEkDPqzqPN0BdXeAwDbT9zKipmMnku6hlrPJ31K44hKwhbZE52XK4BauxTO853i0fUqy8Y4OazG992kGQye7PWEt7DUP4bif5jP2CtumBHhhbp4pqF5Lo8++SlyvDKtZXD7YhjwdGIO+SYwfsnF7T1sLzUDCdmBpcY6k7BE3VtLSCAfODkZBSa4qnSIPLf0Eheb8/l5OLEy9M18EzyNulsr99qYSZ1Cd/8ASFNeQjb9/ddP6lLbakYiTB3HIo/H5aqdj3EyzGVtRDZ0+KMfa09Ml5Pqt6GgREoGrTVeTiumnnAk3K9AVYd4xtJieijcN/ZT1GqGq6c+keSDWCkvJZ7DhlmbRr3VahqHUXMBaGgiYxpJPuqzTb3yG9SB7q19mn1XWjw17Gsa44IcXGfLHNVZ4LXuznVuoQ35NNmrlS8JaRq6Gkhu8D7oZyIuxBHllDlWM5O0/CeiMcC57QNyQPcAEoAHujzRcnU08yuCWWzrFmkE9fkrBTqaoVWuHQKfh92tMpvZXGy2cVfakYuWfvZZ7emIRDLXwUFhUkBOqTBCt5aM/jsgZb42WIqVi7IfFH//2Q==',
};

export const Avatar2 = Template.bind({});
Avatar2.args = {
  size: 150,
  label: '',
  backgroundColor: '',
  color: '',
  imageSource: image,
};
