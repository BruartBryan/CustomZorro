import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from './User';
// @ts-ignore
import icon from './assets/res/img/header/iconHome.png';

@Component({
  selector: 'storybook-page',
  templateUrl: './page.html',
  styleUrls: ['./page.css'],
})
export default class PageComponent {
  @Input()
  cardTitle = 'Hôtel 5 étoiles';
  @Input()
  cardDescription = "Petits fours & douche offerts à l'arrivée";

  user: User | null = null;
  icon = icon;

  cardImgSource = '../../assets/ressources/img/hotel.jpg';

  @Output()
  onLogin = new EventEmitter<string>();
  @Output()
  onLogout = new EventEmitter<string>();
  @Output()
  onCreateAccount = new EventEmitter<string>();

  @Output()
  onSell = new EventEmitter<string>();
  @Output()
  onRent = new EventEmitter<string>();

  doLogout() {
    const message = 'disconnection : ' + this.user?.name;
    this.user = null;
    this.onLogin.emit(message);
  }

  doLogin() {
    let message = '';
    if (this.user?.name) {
      message = 'log in tentative: ' + this.user.name;
    } else {
      message = 'log in tentative: no existing user';
    }
    this.onLogin.emit(message);
  }

  doCreateAccount(event: any) {
    this.user = { name: 'Andrew Brown' };
    const message = 'account created: ' + this.user.name;
    this.onCreateAccount.emit(message);
  }

  sellClick() {
    let message = '';
    if (this.user) {
      message = 'cta : sell by user:' + this.user?.name;
    } else {
      message = 'cta: sell by anonymous';
    }

    this.onSell.emit(message);
  }

  rentClick() {
    let message = '';
    if (this.user) {
      message = 'cta : rent by user:' + this.user?.name;
    } else {
      message = 'cta: rent by anonymous';
    }
    this.onRent.emit(message);
  }
}
